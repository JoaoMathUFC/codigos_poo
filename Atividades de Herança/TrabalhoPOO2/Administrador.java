package TrabalhoPOO2;

public class Administrador extends Empregado {
	
	private double ajudaDeCusto;
	
	
	public Administrador() {
		
	}
	public Administrador(String nome,String endere�o,String telefone,int codigoSetor,double salarioBase,double imposto,double ajudaDeCusto) {
		super(nome,endere�o,telefone,codigoSetor,salarioBase,imposto);
		this.ajudaDeCusto = ajudaDeCusto;
		
	}

	public double getAjudaDeCusto() {
		return ajudaDeCusto;
	}

	public void setAjudaDeCusto(double ajudaDeCusto) {
		this.ajudaDeCusto = ajudaDeCusto;
	}
	
	public double salarioAdm() {
		
		double salarioAdm = calcularSalario() + ajudaDeCusto;
		
		return salarioAdm;
		
	}
	@Override
	public String toString() {
		return "x-x-x-x-x-ADMINISTRADOR-x-x-x-x-x-x"+"\n\n" + "Nome :"+ getNome()+"\n" + "Endere�o :"+ getEndere�o()+"\n"  + "Telefone :"+ getTelefone()+"\n" + "Codigo do Setor :"+ getCodigoSetor()+"\n"  +"Sal�rio Base :" + getSalarioBase() +"\n"+ "Imposto :" + getImposto() +"%" +"\n"+"Ajuda De Custo :" + ajudaDeCusto + "\n"+"Sal�rio Do Administrador :"+ salarioAdm() + "\n\n";
	}

}
