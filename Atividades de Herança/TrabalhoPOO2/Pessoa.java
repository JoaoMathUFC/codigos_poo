package TrabalhoPOO2;

public class Pessoa {
	
	private String nome;
	private String enderešo;
	private String telefone;
	
	
	
	public Pessoa() {
		
	}
	public Pessoa(String nome,String enderešo,String telefone) {
		this.nome = nome;
		this.enderešo = enderešo;
		this.telefone = telefone;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEnderešo() {
		return enderešo;
	}
	public void setEnderešo(String enderešo) {
		this.enderešo = enderešo;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	@Override
	public String toString() {
		return "x-x-x-x-x-PESSOA-x-x-x-x-x-x"+"\n\n"+"Nome :" + nome + "\n" +"Enderešo :" + enderešo + "\n" + "Telefone :" + telefone + "\n\n";
	}
	
	
	
}
