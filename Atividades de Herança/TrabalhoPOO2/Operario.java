package TrabalhoPOO2;

public class Operario extends Empregado {
	
	private double valorProdu�ao;
	private double comissao;
	
	public Operario() {
		
	}
	public Operario(String nome,String endere�o,String telefone,int codigoSetor,double salarioBase,double imposto,double valorProdu�ao,double comissao) {
		
		super(nome,endere�o,telefone,codigoSetor,salarioBase,imposto);
		this.valorProdu�ao = valorProdu�ao;
		this.comissao = comissao;
		
	}
	public double getValorProdu�ao() {
		return valorProdu�ao;
	}
	public void setValorProdu�ao(double valorProdu�ao) {
		this.valorProdu�ao = valorProdu�ao;
	}
	public double getComissao() {
		return comissao;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	
	public double salarioOperario(){
		
		double salarioOperario = calcularSalario() + ((valorProdu�ao*comissao)/100);
		
		return salarioOperario;
	}
	@Override
	public String toString() {
		return "x-x-x-x-x-OPER�RIO-x-x-x-x-x-x"+"\n\n" +"Nome :"+ getNome()+"\n" + "Endere�o :"+ getEndere�o()+"\n"  + "Telefone :"+ getTelefone()+"\n" + "Codigo do Setor :"+ getCodigoSetor()+"\n"+"Sal�rio Base :" + getSalarioBase() +"\n"+ "Imposto :" + getImposto() +"%" +"\n"+"Valor De Produ��o :" + valorProdu�ao +"\n"+ "Comiss�o :" + comissao+"%" +"\n"+"Sal�rio Do Oper�rio :"+ salarioOperario() + "\n\n";
	}
	
	
}
