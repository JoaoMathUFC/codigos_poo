package TrabalhoPOO2;

public class Fornecedor extends Pessoa {
	
	private double valorCredito;
	private double valorDivida;
	
	
	public Fornecedor() {
		
	}
	public Fornecedor(String nome,String enderešo,String telefone,double valorCredito,double valorDivida) {
		
	super(nome,enderešo,telefone);
	this.valorCredito = valorCredito;
	this.valorDivida = valorDivida;	
		
	}
	public double getValorCredito() {
		return valorCredito;
	}
	public void setValorCredito(double valorCredito) {
		this.valorCredito = valorCredito;
	}
	public double getValorDivida() {
		return valorDivida;
	}
	public void setValorDivida(double valorDivida) {
		this.valorDivida = valorDivida;
	}
	
	public double obterSaldo() {
		
		double saldo = valorCredito - valorDivida;
		
		return saldo;
		
	}
	@Override
	public String toString() {
		return "x-x-x-x-x-FORNECEDOR-x-x-x-x-x-x"+"\n\n" +"Nome :"+ getNome()+"\n" + "Enderešo :"+ getEnderešo()+"\n"  + "Telefone :"+ getTelefone()+"\n"+ "Valor De Credito :" + valorCredito +"\n" + "Valor Da Divida :" + valorDivida +"\n"+"Saldo :" +obterSaldo() + "\n\n";
	}
	

}
