package TrabalhoPOO2;

public class Empregado extends Pessoa {
	
	private int codigoSetor;
	private double salarioBase;
	private double imposto;
	
	
	
	public Empregado() {
		
	}
	public Empregado(String nome,String endere�o,String telefone,int codigoSetor,double salarioBase,double imposto) {
		super(nome,endere�o,telefone);
		this.codigoSetor = codigoSetor;
		this.salarioBase = salarioBase;
		this.imposto = imposto;
		
		
	}
	public int getCodigoSetor() {
		return codigoSetor;
	}
	public void setCodigoSetor(int codigoSetor) {
		this.codigoSetor = codigoSetor;
	}
	public double getSalarioBase() {
		return salarioBase;
	}
	public void setSalarioBase(double salarioBase) {
		this.salarioBase = salarioBase;
	}
	public double getImposto() {
		return imposto;
	}
	public void setImposto(double imposto) {
		this.imposto = imposto;
	}
	
	
	public double calcularSalario() {
		double salario = salarioBase - ((salarioBase*imposto)/100);
		
		return salario;
	}
	@Override
	public String toString() {
		return "x-x-x-x-x-EMPREGADO-x-x-x-x-x-x"+"\n\n" +"Nome :"+ getNome()+"\n" + "Endere�o :"+ getEndere�o()+"\n"  + "Telefone :"+ getTelefone()+"\n" + "Cod�go Setor :" + codigoSetor + "\n"+ "Sal�rio Base :" + salarioBase +"\n"+ "Imposto :" + imposto +"%" +"\n"+"Salario :"+ calcularSalario() + "\n\n";
	}

}
