package TrabalhoPOO2;

public class Vendedor extends Empregado {
	
	private double valorVendas;
	private double comissao;
	
	public Vendedor() {
		
	}
	public Vendedor(String nome,String endere�o,String telefone,int codigoSetor,double salarioBase,double imposto,double valorVendas,double comissao) {
		
		super(nome,endere�o,telefone,codigoSetor,salarioBase,imposto);
		this.valorVendas = valorVendas;
		this.comissao = comissao;
		
	}
	public double getValorVendas() {
		return valorVendas;
	}
	public void setValorVendas(double valorVendas) {
		this.valorVendas = valorVendas;
	}
	public double getComissao() {
		return comissao;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	
	public double salarioVendedor() {
		
		double salarioVendedor = calcularSalario() + ((valorVendas*comissao)/100);
		
		return salarioVendedor;
	}
	@Override
	public String toString() {
		return "x-x-x-x-x-VENDEDOR-x-x-x-x-x-x"+"\n\n" +"Nome :"+ getNome()+"\n" + "Endere�o :"+ getEndere�o()+"\n"  + "Telefone :"+ getTelefone()+"\n" + "Codigo do Setor :"+ getCodigoSetor()+"\n"+"Sal�rio Base :" + getSalarioBase() +"\n"+ "Imposto :" + getImposto() +"%" +"\n"+"Valor Vendas :" + valorVendas + "\n"+"Comiss�o :" + comissao+"%"+"\n" +"Sal�rio Do Vendedor :"+ salarioVendedor() + "\n\n";
	}
	

}
