package br.ufc.quixada.cc.model;
import java.util.*;

public class Poupanca extends ContaBancaria implements Comprovante {
	Scanner scan = new Scanner(System.in);
	private double limite;

	public Poupanca() {
		System.out.println("Qual o Numero da sua Conta?");
		double numcont = scan.nextDouble();
		
		super.setNumconta(numcont);
		this.limite =600;
		System.out.println(" X------X------------X------------X------X");
		System.out.println(" |                                       |");
		System.out.println(" |       Conta Adicionada Com Sucesso    |");
		System.out.println(" |                                       |");
		System.out.println(" X------X------------X------------X------X");
	
	}
	
	@Override
	public double Sacar(double a) {
				
		if(a <= limite){
			super.setSaldo(super.getSaldo()- a);
			
			return super.getSaldo();
		}
		else
		return a;
	}

	@Override
	public double Depositar(double a) {
		
		super.setSaldo(super.getSaldo()+ a);
		System.out.println(" X------X------------X------------X------X");
		System.out.println(" |                                       |");
		System.out.println(" |       Deposito Feito Com Sucesso      |");
		System.out.println(" |                                       |");
		System.out.println(" X------X------------X------------X------X");
		mostrarDados();
		return super.getSaldo();
	}
	
	public Poupanca(double numconta,double saldo,double limite) {
		super(numconta,saldo);
		this.limite = limite;
		
	}

	@Override
	public void mostrarDados() {
		System.out.println("\nX--------------------X\n"+"Poupan�a"+"\n\n"+"Numero da Conta:"+this.getNumconta() +"\n"+"Saldo: R$ "+this.getSaldo()+"\n"+ "Limite:"+ limite+"\nX--------------------X\n");
	}

	public double getLimite() {
		return limite;
	}

	public void setLimite(double limite) {
		this.limite = limite;
	}


	@Override
	public void Transferir(double valor,ContaBancaria conta) {

		Sacar(valor);
		conta.Depositar(valor);
	
		
	}


}
