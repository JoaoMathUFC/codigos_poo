package br.ufc.quixada.cc.model;
import java.util.*;
public class Corrente extends ContaBancaria implements Comprovante {
	Scanner scan = new Scanner(System.in);
	
	private double taxadeOpe;
	
	
	public Corrente() {
		System.out.println("Qual o Numero da sua Conta?");
		double numcont = scan.nextDouble();
		
		super.setNumconta(numcont);
		this.taxadeOpe = 1;
		System.out.println(" X------X------------X------------X------X");
		System.out.println(" |                                       |");
		System.out.println(" |       Conta Adicionada Com Sucesso    |");
		System.out.println(" |                                       |");
		System.out.println(" X------X------------X------------X------X");
	
	}
	public Corrente(double numconta,double saldo) {
		super(numconta,saldo);
		this.taxadeOpe = 1;
	
		
	}
	@Override
	public double Sacar(double a) {
		if(a <= super.getSaldo()) {
			
		super.setSaldo(super.getSaldo() - a - taxadeOpe);
		System.out.println(" X------X------------X------------X------X");
		System.out.println(" |                                       |");
		System.out.println(" |       Saque Feito Com Sucesso         |");
		System.out.println(" |                                       |");
		System.out.println(" X------X------------X------------X------X");
		mostrarDados();
		return super.getSaldo();
		}
		else {
			System.out.println(" X------X------------X------------X------X");
			System.out.println(" |                                       |");
			System.out.println(" |       Impossivel Efetuar o Saque      |");
			System.out.println(" |          Saldo Indisponível           |");
			System.out.println(" |                                       |");
			System.out.println(" X------X------------X------------X------X");
			mostrarDados();
			System.out.println("Digite o Valor Do Saque Novamente:");
			Sacar(scan.nextDouble());
		return a;
		}
	}

	@Override
	public double Depositar(double a) {
			
		super.setSaldo(super.getSaldo()+ a - taxadeOpe);
		
		System.out.println(" X------X------------X------------X------X");
		System.out.println(" |                                       |");
		System.out.println(" |       Deposito Feito Com Sucesso      |");
		System.out.println(" |                                       |");
		System.out.println(" X------X------------X------------X------X");
		mostrarDados();
		return super.getSaldo();
	}

	@Override
	public void mostrarDados() {
		System.out.println("\nX--------------------X\n"+"Corrente"+"\n\n"+"Numero da Conta:"+this.getNumconta() +"\n"+"Saldo: R$ "+this.getSaldo()+"\n"+ "Taxa de Operação:"+ taxadeOpe+"\nX--------------------X\n");
	}

	public double getTaxadeOpe() {
		return taxadeOpe;
	}

	public void setTaxadeOpe(double taxadeOpe) {
		this.taxadeOpe = taxadeOpe;
	}
	@Override
	public void Transferir(double valor,ContaBancaria conta) {
	
			Sacar(valor);
			conta.Depositar(valor);
			
			}

}
