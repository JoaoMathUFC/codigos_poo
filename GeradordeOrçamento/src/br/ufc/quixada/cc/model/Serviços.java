package br.ufc.quixada.cc.model;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JDesktopPane;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Servi�os {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
	            }
	        } catch ( ClassNotFoundException  |InstantiationException |IllegalAccessException |javax.swing.UnsupportedLookAndFeelException ex) {
	            System.err.println(ex);   
	        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Servi�os window = new Servi�os();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Servi�os() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(1366, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnProdutos = new JButton("Produtos");
		btnProdutos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnProdutos.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		btnProdutos.setBounds(392, 206, 590, 102);
		frame.getContentPane().add(btnProdutos);
		
		JButton button = new JButton("Servi\u00E7os");
		button.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		button.setBounds(392, 383, 590, 102);
		frame.getContentPane().add(button);
		
		JButton btnNewButton = new JButton("Retornar");
		btnNewButton.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
					Menu.main(null);
			}
		});
		btnNewButton.setBounds(1122, 635, 182, 60);
		frame.getContentPane().add(btnNewButton);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 1350, 768);
		frame.getContentPane().add(desktopPane);
	}
}
