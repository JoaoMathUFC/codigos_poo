package br.ufc.quixada.cc.model;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JDesktopPane;

public class Menu {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
	            }
	        } catch ( ClassNotFoundException  |InstantiationException |IllegalAccessException |javax.swing.UnsupportedLookAndFeelException ex) {
	            System.err.println(ex);   
	        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(1366, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMenu = new JLabel("Menu");
		lblMenu.setFont(new Font("Tw Cen MT Condensed Extra Bold", Font.PLAIN, 68));
		lblMenu.setBounds(599, 56, 181, 68);
		frame.getContentPane().add(lblMenu);
		
		JButton btnOramentos = new JButton("Or\u00E7amentos");
		btnOramentos.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		btnOramentos.setBounds(454, 233, 461, 68);
		frame.getContentPane().add(btnOramentos);
		
		JButton button = new JButton("Acrescentar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				Servi�os.main(null);
			}
		});
		button.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		button.setBounds(454, 377, 461, 68);
		frame.getContentPane().add(button);
		
		JButton button_1 = new JButton("Remover");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Servi�os.main(null);
			}
		});
		button_1.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		button_1.setBounds(454, 521, 461, 68);
		frame.getContentPane().add(button_1);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 1350, 768);
		frame.getContentPane().add(desktopPane);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Login.main(null);
			}
		});
		btnLogout.setFont(new Font("Tw Cen MT Condensed", Font.PLAIN, 40));
		btnLogout.setBounds(1069, 640, 165, 42);
		desktopPane.add(btnLogout);
	}
}
