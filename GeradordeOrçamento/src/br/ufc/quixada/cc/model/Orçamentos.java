package br.ufc.quixada.cc.model;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class Orçamentos {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
	            }
	        } catch ( ClassNotFoundException  |InstantiationException |IllegalAccessException |javax.swing.UnsupportedLookAndFeelException ex) {
	            System.err.println(ex);   
	        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Orçamentos window = new Orçamentos();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Orçamentos() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setSize(1366, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
