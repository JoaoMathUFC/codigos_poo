package br.ufc.quixada.cc.model;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.JDesktopPane;

public class Login {

	private JFrame frame;
	private JTextField textField;
	private JPasswordField txtsenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
	            }
	        } catch ( ClassNotFoundException  |InstantiationException |IllegalAccessException |javax.swing.UnsupportedLookAndFeelException ex) {
	            System.err.println(ex);   
	        }
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.menu);
		frame.setSize(800, 600);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblLogin = new JLabel("Login :");
		lblLogin.setBounds(225, 305, 86, 39);
		lblLogin.setFont(new Font("Tahoma", Font.PLAIN, 17));
		frame.getContentPane().add(lblLogin);
		
		JLabel lblSenha = new JLabel("Senha :");
		lblSenha.setBounds(225, 384, 92, 39);
		lblSenha.setFont(new Font("Tahoma", Font.PLAIN, 17));
		frame.getContentPane().add(lblSenha);
		
		JLabel label = new JLabel("");
		label.setBounds(283, 11, 260, 229);
		label.setIcon(new ImageIcon("C:\\Users\\Jo\u00E3o Matheus\\Pictures\\Projeto final\\money-bag.png"));
		frame.getContentPane().add(label);
		
		textField = new JTextField();
		textField.setBounds(288, 305, 233, 32);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(343, 482, 114, 39);
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(checkLogin(textField.getText(),new String(txtsenha.getPassword()))){
				
			//	Menu mn = new Menu();
				Menu.main(null);
			 
				
				} 
				else {
					JOptionPane.showMessageDialog(null, "Dados Invalidos");
					textField.setText(null);
					txtsenha.setText(null);
				}
				
				
			}
		});
		btnEntrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		frame.getContentPane().add(btnEntrar);
		
		txtsenha = new JPasswordField();
		txtsenha.setBounds(288, 390, 233, 32);
		frame.getContentPane().add(txtsenha);
		
		JDesktopPane desktopPane = new JDesktopPane();
		desktopPane.setBounds(0, 0, 784, 561);
		frame.getContentPane().add(desktopPane);
		
	}
	
	public boolean checkLogin(String login,String senha) {
		return login.equals("admin") && senha.equals("admin");
	}
}
